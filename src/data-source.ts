import 'reflect-metadata';
import { DataSource } from 'typeorm';
import { User } from './entity/User';
import { City} from './entity/city';
import { Country } from './entity/country';

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'root12345',
  database: 'db1',
  synchronize: true,
  logging: true,
  entities: ["src/entity/*.ts"],
});
AppDataSource.initialize()
  .then(async () => {
    console.log('Data source initialized');
  })
  .catch((error) => console.log(error));

