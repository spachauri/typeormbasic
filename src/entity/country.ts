import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { City } from './city';

@Entity()
export class Country {
  @PrimaryGeneratedColumn()
  countryid: number;

  @Column()
  name: string;

  @OneToMany((type) => City, (City) => City.country, { cascade: true })
  cities: City[];
}
