import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { Country } from './country';

@Entity()
export class City {
  @PrimaryGeneratedColumn()
  cityid: number;

  @Column()
  name: string;

  @ManyToOne((type) => Country, (Country) => Country.cities)
  country: Country;
}
