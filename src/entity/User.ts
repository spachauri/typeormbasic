import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from 'typeorm';
import { Details } from './Details';

@Entity()
export class User {
  @Column()
  name: string;

  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(type=>Details)
  @JoinColumn()
  refCol:Details
}
