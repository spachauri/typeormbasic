import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from 'typeorm';

@Entity()
export class Details {
  @Column()
  name: string;

  @PrimaryGeneratedColumn()
  id: number;

}