import * as readline from 'readline';
import { stdin as input, stdout as output } from 'node:process';
import { AppDataSource } from './data-source';
import { User } from './entity/User';
import { City } from './entity/city';
import { Country } from './entity/country';
import { Details } from './entity/Details';
import { Student } from './entity/student';
import { Course } from './entity/course';

// const user = new User();
AppDataSource.initialize()
  .then(async () => {
    console.log('Data source initialized');
  })
  .catch((error) => console.log(error));
async function toFindUserById(id) {
  const Ansuser = await AppDataSource.createQueryBuilder()
    .select('user')
    .from(User, 'user')
    .where('user.id = :id', { id: `${id}` })
    .getOne();
  console.log(Ansuser);
}
async function toShowAllUser() {
  const Ansuser = await AppDataSource.createQueryBuilder()
    .select('user')
    .from(User, 'user')
    .execute();
  console.log(Ansuser);
}

async function todeleteUserById(id) {
  await AppDataSource.createQueryBuilder()
    .delete()
    .from(User)
    .where('id = :id', { id: `${id}` })
    .execute();
}

async function saveOneToMany() {
  const City1 = new City();
  City1.cityid = 1;
  City1.name = 'Kolkata';
  await AppDataSource.manager.save(City1);

  const City2 = new City();
  City2.cityid = 2;
  City2.name = 'Jaipur';
  await AppDataSource.manager.save(City2);

  const country = new Country();
  (country.countryid = 1), (country.name = 'India');
  country.cities = [City1, City2];
  await AppDataSource.manager.save(country);
}

async function leftJoinOneToMany() {
  const users = await AppDataSource.getRepository(Country)
    .createQueryBuilder('Country')
    .leftJoinAndSelect('Country.cities', 'city')
    .getMany();
}

async function saveoneOne() {
  const user = new User();
  user.id = 1;
  user.name = 'Shrasti';
  await AppDataSource.manager.save(user);

  const det = new Details();
  det.name = 'Aadhar';
  det.id = 1;
  await AppDataSource.manager.save(det);

  const det2 = new Details();
  det2.name = 'Aadhar';
  det2.id = 2;
  await AppDataSource.manager.save(det2);
}
async function saveManyMany() {
  const stu1 = new Student();
  stu1.id = 1;
  stu1.name = 'Shrasti';
  await AppDataSource.manager.save(stu1);

  const stu2 = new Student();
  stu2.id = 2;
  stu2.name = 'Shrianant';
  await AppDataSource.manager.save(stu2);

  const course1 = new Course();
  course1.id = 1;
  course1.name = 'Hindi';
  await AppDataSource.manager.save(course1);

  const course2 = new Course();
  course2.id = 2;
  course2.name = 'English';

  await AppDataSource.manager.save(course2);
}

const rl = readline.createInterface({ input, output });
rl.setPrompt(
  'Press 1 to see all users data \n Press 2 find data with id \n Press 3 to delete data of given id \n Press 4 to call saveOneToMany'
);
rl.prompt();

(async () => {
  await rl.on('line', (ans) => {
    if (ans === '1') {
      toShowAllUser();
    } else if (ans === '2') {
      rl.prompt(true);
      rl.question('Enter the id of the user', (id) => {
        toFindUserById(id);
      });
    } else if (ans === '3') {
      rl.prompt(true);
      rl.question('Enter the id of the user', (id) => {
        todeleteUserById(id);
      });
    } else if (ans === '4') {
      rl.prompt(true);
      saveOneToMany();
    } else if (ans === '5') {
      rl.prompt(true);
      leftJoinOneToMany();
    } else if (ans === '6') {
      rl.prompt(true);
      saveoneOne();
    } else if (ans === '7') {
      rl.prompt(true);
      saveManyMany();
    } else {
      console.log('you have provided wrong user input');
      rl.close();
    }
  });
})();
